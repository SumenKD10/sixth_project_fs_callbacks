const {
  readTheFile,
  convertMakeAndStoreUpperCase,
  lowerCaseSentence,
  sortedFile,
  deleteAllFiles,
} = require("../problem2.cjs");

readTheFile(function () {
  convertMakeAndStoreUpperCase(function () {
    lowerCaseSentence(function () {
      sortedFile(function () {
        deleteAllFiles((error) => {
          if (error) {
            throw error;
          } else {
            console.log("Operation Successfull");
          }
        });
      });
    });
  });
});
