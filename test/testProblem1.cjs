const {
  createDirectory,
  createRandomFiles,
  deleteAllFiles,
} = require("../problem1.cjs");

let randomNumber = Math.floor(Math.random() * (20 - 1) + 1);
console.log("Random Files Generated :" + randomNumber);
let count = 0;
let arrayFormed = Array(randomNumber)
  .fill(0)
  .map((index) => {
    index = ++count;
    return index;
  });

let pathName = __dirname + "/../Random_JSON_Directory";
createDirectory(pathName, function () {
  createRandomFiles(pathName, arrayFormed, function () {
    deleteAllFiles(pathName, arrayFormed, (errorGot) => {
      if (errorGot) {
        throw errorGot;
      } else {
        console.log("Operation Completed Successfully.");
      }
    });
  });
});
