let fs = require("fs");

function createDirectory(pathName, callBackFuncCreateDirectory) {
  fs.mkdir(pathName, (errorGot) => {
    if (errorGot) {
      console.error("Error:");
      console.error(errorGot);
    } else {
      console.log("Directory Created Successfully.");
      callBackFuncCreateDirectory();
    }
  });
}

function createRandomFiles(pathName, arrayPassed, callBackFuncRandomFiles) {
  let countFiles = 0;
  let totalFiles = arrayPassed.length;
  arrayPassed.forEach((eachData) => {
    let objectCreated = {
      Name: "RandomName" + eachData,
      Value: "RandomValue" + eachData,
    };
    let jsonString = JSON.stringify(objectCreated);
    let filePath = pathName + "/Random_JSONFile" + eachData + ".json";
    fs.writeFile(filePath, jsonString, (errorGot) => {
      if (errorGot) {
        console.error("Error:");
        console.error(errorGot);
      } else {
        console.log("File Created SuccessFully");
        countFiles++;
        if (countFiles === totalFiles) {
          callBackFuncRandomFiles();
        }
      }
    });
  });
}

function deleteAllFiles(pathName, arrayPassed, callBackFuncDeleteFiles) {
  let countFiles = 0;
  let totalFiles = arrayPassed.length;
  arrayPassed.forEach((eachData) => {
    let filePath = pathName + "/Random_JSONFile" + eachData + ".json";
    fs.unlink(filePath, (errorGot) => {
      if (errorGot) {
        console.error("Error:");
        console.error(errorGot);
      } else {
        console.log("Deleted File Successfully");
        countFiles++;
        if (countFiles === totalFiles) {
          callBackFuncDeleteFiles();
        }
      }
    });
  });
}

module.exports = { createDirectory, createRandomFiles, deleteAllFiles };
