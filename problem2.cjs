let fs = require("fs");
let path = require("path");

function readTheFile(callBackReadTheFile) {
  fs.readFile("lipsum.txt", "utf8", (errorGot, data) => {
    if (errorGot) {
      console.error("Error:");
      console.error(errorGot);
    } else {
      console.log("File Read Successfully.");
      callBackReadTheFile();
    }
  });
}

function convertMakeAndStoreUpperCase(callBackUpperCase) {
  fs.readFile("lipsum.txt", "utf8", (errorGot1, data) => {
    if (errorGot1) {
      console.error("Error:");
      console.error(errorGot1);
    } else {
      let dataConverted = data.toUpperCase();
      let upperFileName = "newUpperCaseFile.txt";
      let filePath = __dirname + "/" + upperFileName;
      fs.writeFile(filePath, dataConverted, (errorGot2) => {
        if (errorGot2) {
          console.error("Error:");
          console.error(errorGot2);
        } else {
          console.log("Upper Case File Created SuccessFully");
          fs.appendFile("filenames.txt", upperFileName + "\n", (errorGot3) => {
            if (errorGot3) {
              console.error("Error:");
              console.error(errorGot3);
            } else {
              console.log("Upper Case File Written Inside filenames.txt");
              callBackUpperCase();
            }
          });
        }
      });
    }
  });
}

function lowerCaseSentence(callBackLowerCase) {
  fs.readFile(
    path.join(__dirname, "newUpperCaseFile.txt"),
    "utf8",
    (errorGot1, data) => {
      if (errorGot1) {
        console.error("Error:");
        console.error(errorGot1);
      } else {
        let lowerFileName = "newLowerCaseFile.txt";
        let newData = data.toLowerCase().split(".").join(".\n");
        fs.appendFile(lowerFileName, newData, (errorGot) => {
          if (errorGot) {
            console.error("Error:");
            console.error(errorGot);
          } else {
            console.log("Lower Case File Created SuccessFully");
          }
          fs.appendFile(
            "filenames.txt",
            lowerFileName + "\n",
            (errorGotNew) => {
              if (errorGotNew) {
                console.error("Error:");
                console.error(errorGotNew);
              } else {
                console.log("Lower Case File Written Inside filenames.txt");
                callBackLowerCase();
              }
            }
          );
        });
      }
    }
  );
}

function sortedFile(callBackSortedFile) {
  fs.readFile(
    path.join(__dirname, "newLowerCaseFile.txt"),
    "utf8",
    (errorGot, data) => {
      if (errorGot) {
        console.error("Error:");
        console.error(errorGot);
      } else {
        let newData = data.split("\n").sort();
        //console.log(newData);
        let sortedFileName = "sortedFile.txt";
        fs.appendFile(
          sortedFileName,
          newData.join("\n").toString(),
          (errorGotNew1) => {
            if (errorGotNew1) {
              console.error("Error:");
              console.error(errorGotNew1);
            } else {
              console.log("Sorted File Created SuccessFully");
              fs.appendFile("filenames.txt", sortedFileName, (errorGotNew2) => {
                if (errorGotNew2) {
                  console.error("Error:");
                  console.error(errorGotNew2);
                } else {
                  console.log("Sorted File Written Inside filenames.txt");
                  callBackSortedFile();
                }
              });
            }
          }
        );
      }
    }
  );
}

function deleteAllFiles(callBackDeleting) {
  fs.readFile(
    path.join(__dirname, "filenames.txt"),
    "utf8",
    (errorGot, data) => {
      if (errorGot) {
        console.error("Error:");
        console.error(errorGot);
      } else {
        let countFiles = 0;
        let filenamesData = data.split("\n");
        let totalFilesCount = filenamesData.length;
        let deleteFileArray = filenamesData.forEach((eachData) => {
          let filePath = __dirname + "/" + eachData;
          fs.unlink(filePath, (errorGotInside) => {
            if (errorGotInside) {
              console.error("Error:");
              console.error(errorGotInside);
            } else {
              console.log("File Deleted");
              countFiles++;
              if (countFiles === totalFilesCount) {
                callBackDeleting();
              }
            }
          });
        });
      }
    }
  );
}

//readTheFile();
//convertMakeAndStoreUpperCase();
//lowerCaseSentence();
//sortedFile();
//deleteAllFiles();

module.exports = {
  readTheFile,
  convertMakeAndStoreUpperCase,
  lowerCaseSentence,
  sortedFile,
  deleteAllFiles,
};
